/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-12-01 14:34:21 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-12-01 14:41:37
 */
#include "BoundedBuffer.h"
#include <string>
#include <queue>

using namespace std;

BoundedBuffer::BoundedBuffer(int _cap):buff_size(_cap) {
	
}

BoundedBuffer::~BoundedBuffer() {
	pthread_mutex_destroy(&mutx);
	pthread_cond_destroy(&consumer_var);
	pthread_cond_destroy(&producer_var);
}

int BoundedBuffer::size() {return q.size();}

void BoundedBuffer::push(string str) {
    pthread_mutex_lock(&mutx);
    while (this->size()>=this->buff_size)
	    pthread_cond_wait(&consumer_var, &mutx);
	q.push (str);
    pthread_cond_broadcast(&producer_var);
    pthread_mutex_unlock(&mutx);
}

string BoundedBuffer::pop() {
	pthread_mutex_lock(&mutx);
	while (q.empty())
		/* wait on condition variable */
		this->ret = pthread_cond_wait(&producer_var, &mutx);
	string s = q.front();
	q.pop();
    pthread_cond_broadcast(&consumer_var);
    pthread_mutex_unlock(&mutx);
    return s;
}
