/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-12-01 14:34:21 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-12-01 14:41:37
 */
#ifndef Histogram_h
#define Histogram_h

#include <queue>
#include <string>
#include <vector>
#include <thread>
#include <unordered_map>

using namespace std;

class Histogram {
private:
    pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
	int hist [3][10];					// histogram for each person with 10 bins each
	unordered_map<string, int> map;  	// person name to index mapping
	vector<string> names; 				// names of the 3 persons
public:
    Histogram();
	void update (string, string); 		// updates the histogram
    void print();						// prints the histogram
};

#endif 
