/*
 * @Author: Jason A. Kolodziej
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved.
 * @Date: 2018-11-14 09:54:00
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-11-14 13:44:42
 */

#ifndef MQREQUESTCHANNEL_H
#define MQREQUESTCHANNEL_H

#include "reqchannel.h"



class MQRequestChannel : public RequestChannel {

private:
    struct mq_msgbuf {
        long to;
        long from;
        char mtext[MSGBUFF_SIZE];
    } mbuff;
//    struct message {
//        long mtype;
//        struct msg_txt {
//            int qid;
//            char buf [MSGBUFF_SIZE];
//        }msg_txt;
//    };
    //Side     my_side;
    key_t 	key;
    int 	qid;

public:

    /* -- CONSTRUCTOR/DESTRUCTOR */
    const int id;
    explicit MQRequestChannel(const Side, const int);
    /* Creates a "local copy" of the channel specified by the given name.
     If the channel does not exist, the associated IPC mechanisms are
     created. If the channel exists already, this object is associated with the channel.
     The channel has two ends, which are conveniently called "SERVER_SIDE" and "CLIENT_SIDE".
     If two processes connect through a channel, one has to connect on the server side
     and the other on the client side. Otherwise the results are unpredictable.

     NOTE: If the creation of the request channel fails (typically happens when too many
     request channels are being created) and error message is displayed, and the program
     unceremoniously exits.

     NOTE: It is easy to open too many request channels in parallel. In most systems,
     limits on the number of open files per process limit the number of established
     request channels to 125.
    */

    ~MQRequestChannel( ) override;

    /* Destructor of the local copy of the bus. By default, the Server Side deletes any IPC
     mechanisms associated with the channel. */

    string cread( );
    /* Blocking read of data from the channel. Returns a string of characters
     read from the channel. Returns NULL if read failed. */

    void cwrite(string _msg);
    /* Write the data to the channel. The function returns NULL */
};


#endif //PA6_MQREQUESTCHANNEL_H
