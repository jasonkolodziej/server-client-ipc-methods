/*
 * @Author: Jason A. Kolodziej
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved.
 * @Date: 2018-11-14 09:54:00
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-11-14 13:44:42
 */

#include "MQRequestChannel.h"

/*--------------------------------------------------------------------------*/
/* DEFINITIONS FOR FUNCTIONS OF     MQRequestChannel					    */
/*--------------------------------------------------------------------------*/

MQRequestChannel::MQRequestChannel(const Side _side, const int _id):RequestChannel(_side), id(_id){
    // ftok to generate unique key
    key = ftok(PATHNAME, id);
    if(_side==RequestChannel::SERVER_SIDE){
        if ((qid = msgget (key, IPC_CREAT | QUEUE_PERMISSIONS)) == -1) {
            const string e = "server => msgget key id : "+to_string(key);
            perror(e.c_str());
        }
    }
    else{
        if ((qid = msgget (key, 0)) == -1) {
            const string ee = "client => msgget key id : "+to_string(key);
            perror (ee.c_str());
        }
    }
}

string MQRequestChannel::cread(void){
    if (RequestChannel::my_side==CLIENT_SIDE){
        const string side = "client";
        if(msgrcv(qid, &mbuff, sizeof(mbuff), CLIENT_MESSAGE, 0)==-1){
            if (errno == EINVAL || errno == EFAULT){
                const string e = "failed to receive buffer! =>" + side
                                 + " : with key = " + to_string(key) + ", errno " + to_string(errno);
                perror(e.c_str());
            }
            else{
                const string e = "failed to receive buffer! =>" + side
                                 + " : with key = " +to_string(key);
                perror(e.c_str());
            }
        }
    }
    else{
        const string side = "server";
        if( msgrcv(qid, &mbuff, sizeof(mbuff.mtext), SERVER_MESSAGE, 0) == -1) {
            if (errno == EINVAL || errno == EFAULT){
                const string e = "failed to receive buffer! =>" + side
                                 + " : with key = " + to_string(key) + ", errno " + to_string(errno);
                perror(e.c_str());
            }
            else{
                const string e = "failed to receive buffer! =>" + side
                                 + " : with key = " +to_string(key);
                perror(e.c_str());
            }
        }
    }

    return string(mbuff.mtext);
}

void MQRequestChannel::cwrite(string msg){
    if (RequestChannel::my_side==CLIENT_SIDE){
        mbuff.to = SERVER_MESSAGE;
        mbuff.from = CLIENT_MESSAGE;
    }
    else{
        mbuff.to =CLIENT_MESSAGE;
        mbuff.from =SERVER_MESSAGE;
    }
    strcpy(mbuff.mtext,msg.c_str());
    if (msgsnd (qid, &mbuff, sizeof(mbuff.mtext), 0) == -1) {
        if (errno == EINVAL || errno == EFAULT){
            const string side = ((RequestChannel::my_side == SERVER_SIDE) ? "server" : "client");
            const string e = "failed to send buffer! =>" + side
                             + " : with key = " + to_string(key) + ", errno " + to_string(errno);
            perror(e.c_str());
        }
        else{
            const string side = ((RequestChannel::my_side == SERVER_SIDE) ? "server" : "client");
            const string e = "failed to send buffer! =>" + side
                             + " : with key = " +to_string(key);
            perror(e.c_str());
        }
    }
}

MQRequestChannel::~MQRequestChannel() {
    if (RequestChannel::my_side==SERVER_SIDE) msgctl(qid, IPC_RMID, (struct msqid_ds *) 0); // delete the msg queue
}


/*--------------------------------------------------------------------------*/
/* END OF THE   MQRequestChannel    CLASS								    */
/*--------------------------------------------------------------------------*/