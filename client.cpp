/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2019-02-16 09:41:50 
 * @Last Modified by:   Jason A. Kolodziej 
 * @Last Modified time: 2019-02-16 09:41:50 
 */

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <sstream>
#include <iomanip>

#include <sys/time.h>
#include <cassert>
#include <assert.h>

#include <cmath>
#include <numeric>
#include <algorithm>

#include <list>
#include <vector>

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

#include <signal.h>

#include "reqchannel.h"
#include "FIFORequestChannel.h"
#include "SHMRequestChannel.h"
#include "MQRequestChannel.h"
#include "NetworkRequestChannel.h"
#include "BoundedBuffer.h"
#include "Histogram.h"
using namespace std;

struct request_thread_args
{
    BoundedBuffer* struct_buff;
    string param;
    int value=0;
};

struct worker_thread_args
{
    RequestChannel* w_channel;
    BoundedBuffer* john_smith_buff;
    BoundedBuffer* jane_smith_buff;
    BoundedBuffer* joe_smith_buff;
    BoundedBuffer* struct_buff;
    char n;
};

struct svc_thread_args
{
    BoundedBuffer* smith_buff;
    string who;
    Histogram* hist;
};


void* request_thread_function(void* arg) {
    request_thread_args* input = (request_thread_args*) arg;

    for(int i=0; i < (input->value);++i) {
     // cout << "Requesting " << input->param << endl;
        (input->struct_buff)->push(input->param);
    }
    pthread_exit(NULL);
}

void* worker_thread_function(void* arg) {
    worker_thread_args* input = (worker_thread_args*) arg;
    cout << "input n is -> " <<input->n << endl;
    while(true) {
        //next line retrieves head of buffer AND removes it
        std::string request = (input->struct_buff)->pop();
        if(request == "quit" && input->n == 'n'){
            break;
        }
        else{
            (input->w_channel)->cwrite(request);
        //cout << "Popped " << request << endl;
        if(request == "quit") {
            delete input->w_channel;
            break;
        }
        else{
            std::string response = (input->w_channel)->cread();
           //cout << "Pushing " << response <<  " for " << request << endl;
            if(request == "data John Smith")
                (input->john_smith_buff)->push(response);
            else if(request == "data Jane Smith")
                (input->jane_smith_buff)->push(response);
            else if(request == "data Joe Smith")
                (input->joe_smith_buff)->push(response);
            }
        }
    }
    cout << "Exiting worker thread()\n";
}

void* stat_thread_function(void* arg) {
    svc_thread_args* input=(svc_thread_args*) arg;
    //int i = 0;
    while(true){
        std::string val = (input->smith_buff)->pop();
        if(val=="quit") break;
        //cout << "Updating " << (input->who) << " With value " << val << " at " << i << endl;
        (input->hist)->update((input->who),val);
        //++i;
    }
}

//////////////HISTOGRAM UPDATOR///////////////////////////////
struct histo_print{Histogram* h; const char * done; };
const void * to_print;
static void on_alarm(int signum)
{
    histo_print* h = (histo_print*)to_print;
    if (*(h->done)) abort();
    system("clear");
    h->h->print();
    //  // Reschedule alarm
    alarm(2);
}

//////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------*/
/* MAIN FUNCTION */
/*--------------------------------------------------------------------------*/

int main(int argc, char * argv[]) {
    int n = 100; //default number of requests per "patient"
    int w = 1; //default number of worker threads
    int b = 3 * n; // default capacity of the request buffer, you should change this default
    char i = 'f'; // default to fifo type
    string host = "";
    int port = -1;
    int opt = 0;
    while ((opt = getopt(argc, argv, "n:w:b:i:h:p:")) != -1) {
        switch (opt) {
            case 'n':
                n = atoi(optarg);
                break;
            case 'w':
                w = atoi(optarg); //This won't do a whole lot until you fill in the worker thread function
                break;
            case 'b':
                b = atoi (optarg);
                break;
            case 'i':
                i = *optarg;
                break;
            case 'p':
                port = atoi (optarg);
                break;
            case 'h':
                host.assign(optarg);
                break;
        }
    }
    if (i=='n' && port==-1) {
			printf("Running client over network requires argument '-p', port number, to be defined!\n");
			exit(-1);
	}
	else if (i=='n' && host=="") {
			printf("Running client over network requires argument '-h', contacting host, to be defined!\n");
			exit(-3);
	}
    int pid = fork();
    if (pid == 0){
        //execl("dataserver", (char*) NULL);
        ///TODO: Resume before submission!
       // execl("dataserver", "dataserver", i.c_str(),(char*) 0);

    }
    else {
        // cout << "starting up server..." << endl;
        // sleep(5);
        cout << "n == " << n << endl;
        cout << "w == " << w << endl;
        cout << "b == " << b << endl;
        string i_is = (i == 'f') ? "fifo type" : (i == 's') ? "shared memory" : (i == 'q') ? "message queue" :
                (i == 'n') ? "network request" : "unknown type";
        cout << "i == " << i << " :=> " << i_is << endl;
        if(i=='n') cout << "h == " << host << endl << "p == " << port << endl;
        BoundedBuffer request_buffer(b);
        BoundedBuffer john_buffer(ceil((float)b/3));
        BoundedBuffer jane_buffer(ceil((float)b/3));
        BoundedBuffer joe_buffer(ceil((float)b/3));

        RequestChannel *chan;//new FIFORequestChannel("control", RequestChannel::CLIENT_SIDE);
        //static int mq_id = 1;

        Histogram hist;
        //static const char what_i_is = i;
        char con = 0;
        histo_print h;
        h.h= &hist;
        h.done = &con;
        //set up the timer
        to_print = &h;
        signal(SIGALRM, &on_alarm);
        alarm(2);  // Setup initial alarm
        // continue till finished

        switch(i){
            case 'f':
                chan = new FIFORequestChannel("control",FIFORequestChannel::CLIENT_SIDE);
                break;
            case 's':
                chan = new SHMRequestChannel(SHMRequestChannel::CLIENT_SIDE,1);
                break;
            case 'q':
                chan = new MQRequestChannel(MQRequestChannel::CLIENT_SIDE, 1);
                break;
            case 'n':
                chan = new NetworkRequestChannel(NetworkRequestChannel::CLIENT_SIDE, port, host);
                break;
            default:
                const string e = "type of client " + to_string(i) + " is not known.";
                perror(e.c_str());
                exit(-1);
        }

        pthread_t r_thread[3];

        //parametrize request threads
        request_thread_args temp_args_john;
        temp_args_john.value=n;
        temp_args_john.struct_buff=&request_buffer;
        temp_args_john.param="data John Smith";

        request_thread_args temp_args_jane;
        temp_args_jane.value=n;
        temp_args_jane.struct_buff=&request_buffer;
        temp_args_jane.param="data Jane Smith";


        request_thread_args temp_args_joe;
        temp_args_joe.value=n;
        temp_args_joe.struct_buff=&request_buffer;
        temp_args_joe.param="data Joe Smith";

        cout << "done linking request threads...\n";

        //dealing with worker threads
        //
        pthread_t w_threads[w];
        worker_thread_args w_arg_arr[w];

        for(int j=0;j < w;++j)
        {
            w_arg_arr[j].n = i;
            if(host!=""){
                
                w_arg_arr[j].w_channel = chan;
            }
            else{
                chan->cwrite("newchannel");
                string s = chan->cread();
                if(i=='f'){ ///FIFO
                    w_arg_arr[j].w_channel = new FIFORequestChannel(s, FIFORequestChannel::CLIENT_SIDE);
                }
                else if(i=='q'){ ///MESSAGE QUEUE
                    int new_id = std::stoi(s);
                    w_arg_arr[j].w_channel = new MQRequestChannel(MQRequestChannel::CLIENT_SIDE,new_id);
                }
                else{ ///SHARED MEMORY
                    int new_id = std::stoi(s);
                    w_arg_arr[j].w_channel = new SHMRequestChannel(SHMRequestChannel::CLIENT_SIDE, new_id);
                }
            }
            w_arg_arr[j].struct_buff = &request_buffer;
            w_arg_arr[j].jane_smith_buff = &jane_buffer;
            w_arg_arr[j].joe_smith_buff = &joe_buffer;
            w_arg_arr[j].john_smith_buff = &john_buffer;
        }
        cout << "done linking worker threads..." << endl;

        //dealing with service/stat threads
        pthread_t svc_thread[3];

        svc_thread_args svc_john;
        svc_thread_args svc_jane;
        svc_thread_args svc_joe;

        svc_john.smith_buff=&john_buffer;
        svc_john.hist=&hist;
        svc_john.who = "data John Smith";

        svc_jane.smith_buff=&jane_buffer;
        svc_jane.hist=&hist;
        svc_jane.who = "data Jane Smith";

        svc_joe.smith_buff=&joe_buffer;
        svc_joe.hist=&hist;
        svc_joe.who = "data Joe Smith";

        cout << "done linking service threads..." <<endl;


        cout << "starting threads..." << endl;
        // KEEP TRACK OF TIME TO EXECUTE
        auto start = chrono::high_resolution_clock::now();
        //creating request threads

        //running through JOHN SMITH
        pthread_create(&r_thread[0],NULL,&request_thread_function,&temp_args_john);
        //running through JANE SMITH
        pthread_create(&r_thread[1],NULL,&request_thread_function,&temp_args_jane);
        //running through JOE SMITH
        pthread_create(&r_thread[2],NULL,&request_thread_function,&temp_args_joe);

        for(int z=0; z<w; ++z) pthread_create(&w_threads[z],NULL,&worker_thread_function,&w_arg_arr[z]);

        pthread_create(&svc_thread[0],NULL,&stat_thread_function,&svc_john);
        pthread_create(&svc_thread[1],NULL,&stat_thread_function,&svc_jane);
        pthread_create(&svc_thread[2],NULL,&stat_thread_function,&svc_joe);

        //joining with the three request threads
        cout << "Done creating all threads" << endl;

        cout << "Joining request threads...\n";
        pthread_join(r_thread[0],NULL);
        pthread_join(r_thread[1],NULL);
        pthread_join(r_thread[2],NULL);
        cout << "done." << endl;


        // finishing the request channels
        cout << "Pushing quit requests...\n";
        for(int ii = 0; ii < w; ++ii) {
            request_buffer.push("quit");
        }
        cout << "done." << endl;

        //joining with worker threads
        cout << "Joining worker threads...\n";
        for(int k=0;k < w;++k)
        {
            pthread_join(w_threads[k],NULL);
        }
        cout << "done." << endl;

        cout << "Pushing quit requests to statistic threads...\n";
        svc_john.smith_buff->push("quit");
        svc_jane.smith_buff->push("quit");
        svc_joe.smith_buff->push("quit");
        cout << "done.\n";

        cout << "Joining service threads...\n";
        pthread_join(svc_thread[0],NULL);
        pthread_join(svc_thread[1],NULL);
        pthread_join(svc_thread[2],NULL);
        cout << "done." << endl;

        // closing the control
        // WILL ALLOW FOR THE SERVER TO EMPTY UNUSED RESORUCES
        cout << "writing quit to server\n";
        chan->cwrite ("quit");
        delete chan;
        system("clear");
        cout << "All Done!!!" << endl;
        //con = 1;
        // END TIME OF EXECUTION
        auto stop = chrono::high_resolution_clock::now();
        auto duration = chrono::duration_cast<chrono::microseconds>(stop - start);
        cout << "Printing Final Histogram...\n";
        hist.print();
        cout<<"Time to completion (milliseconds): "<< duration.count() << endl;
    }
}
