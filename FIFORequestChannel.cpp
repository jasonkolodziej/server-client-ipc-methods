/*
 * @Author: Jason A. Kolodziej
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved.
 * @Date: 2018-11-14 09:54:00
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-11-14 13:44:42
 */
#include "FIFORequestChannel.h"


void EXITONERROR (string msg){
    perror (msg.c_str());
    exit (-1);
}


/*--------------------------------------------------------------------------*/
/* DEFINITIONS FOR FUNCTIONS OF   FIFORequestChannel					    */
/*--------------------------------------------------------------------------*/

std::string FIFORequestChannel::pipe_name(Mode _mode) {
    std::string pname = "fifo_" + my_name;

    if (my_side == CLIENT_SIDE) {
        if (_mode == READ_MODE)
            pname += "1";
        else
            pname += "2";
    }
    else {
        /* SERVER_SIDE */
        if (_mode == READ_MODE)
            pname += "2";
        else
            pname += "1";
    }
    return pname;
}
void FIFORequestChannel::create_pipe (string _pipe_name){
    mkfifo(_pipe_name.c_str(), 0600) < 0; //{
    //	EXITONERROR (_pipe_name);
    //}
}


void FIFORequestChannel::open_write_pipe(string _pipe_name) {
    //if (my_side == SERVER_SIDE)
    create_pipe (_pipe_name);
    wfd = open(_pipe_name.c_str(), O_WRONLY);
    if (wfd < 0) {
        const string e = "open write pipe, " + _pipe_name  + ", side = " + side_name;
        EXITONERROR (e);
    }
}

void FIFORequestChannel::open_read_pipe(string _pipe_name) {
    //if (my_side == SERVER_SIDE)
    create_pipe (_pipe_name);
    rfd = open(_pipe_name.c_str(), O_RDONLY);
    if (rfd < 0) {
        const string e = "open read pipe, " + _pipe_name  + ", side = " + side_name;
        EXITONERROR (e);
        exit (0);
    }
}

FIFORequestChannel::FIFORequestChannel(const string _name, const Side _side):
        my_name(_name), RequestChannel(_side), side_name((_side == RequestChannel::SERVER_SIDE) ? "SERVER" : "CLIENT")
{
    if (_side == SERVER_SIDE) {
        open_write_pipe(pipe_name(WRITE_MODE).c_str());
        open_read_pipe(pipe_name(READ_MODE).c_str());
    }
    else {
        open_read_pipe(pipe_name(READ_MODE).c_str());
        open_write_pipe(pipe_name(WRITE_MODE).c_str());
    }
}

FIFORequestChannel::~FIFORequestChannel() {
    close(wfd);
    close(rfd);
    //if (my_side == SERVER_SIDE) {
    remove(pipe_name(READ_MODE).c_str());
    remove(pipe_name(WRITE_MODE).c_str());
    //}
}



string FIFORequestChannel::cread() {

    char buf [MAX_MESSAGE];
    if (read(rfd, buf, MAX_MESSAGE) <= 0) {
        const string e = "cread : " + name()  + ", side = " + side_name;
        EXITONERROR(e);
    }
    string s = buf;
    return s;

}

void FIFORequestChannel::cwrite(string msg) {

    if (msg.size() > MAX_MESSAGE) {
        const string e = "cwrite, with size : "+ to_string(msg.size()) + " : " + name() + ", side = " + side_name;
        EXITONERROR(e);
    }
    if (write(wfd, msg.c_str(), msg.size()+1) < 0) { // msg.size() + 1 to include the NULL byte
        const string e = "cwrite : " + name()  + ", side = " + side_name;
        EXITONERROR(e);
    }
}

std::string FIFORequestChannel::name() {
    return my_name;
}


int FIFORequestChannel::read_fd() {
    return rfd;
}

int FIFORequestChannel::write_fd() {
    return wfd;
}


/*--------------------------------------------------------------------------*/
/* END OF THE   FIFORequestChannel  CLASS								    */
/*--------------------------------------------------------------------------*/