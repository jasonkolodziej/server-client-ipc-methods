/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2019-02-16 09:42:07 
 * @Last Modified by:   Jason A. Kolodziej 
 * @Last Modified time: 2019-02-16 09:42:07 
 */

#include <cassert>
#include <cstring>
#include <sstream>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>

#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#include "reqchannel.h"
#include "FIFORequestChannel.h"
#include "SHMRequestChannel.h"
#include "MQRequestChannel.h"
#include "NetworkRequestChannel.h"
#include <pthread.h>
using namespace std;


int nchannels = 0;
pthread_mutex_t newchannel_lock;
void* handle_fifo_process_loop (void* _channel);
void* handle_mq_process_loop (void* _channel);
void* handle_shm_process_loop (void* _channel);

//
 void process_new_fifo_channel(RequestChannel* _channel) {
 	nchannels ++;
 	string new_channel_name = "data" + to_string(nchannels) + "_";
 	_channel->cwrite(new_channel_name);
 	RequestChannel * data_channel = new FIFORequestChannel(new_channel_name, RequestChannel::SERVER_SIDE);
 	pthread_t thread_id;
 	if (pthread_create(& thread_id, NULL, handle_fifo_process_loop, data_channel) < 0 ) {
 		EXITONERROR ("");
 	}

 }
void process_new_mq_channel(RequestChannel* _channel) {
	nchannels ++;
	const int new_id = nchannels + 1;
	_channel->cwrite(to_string(new_id));
	RequestChannel* data_channel = new MQRequestChannel(RequestChannel::SERVER_SIDE, new_id);
	pthread_t thread_id;
	if (pthread_create(& thread_id, NULL, handle_mq_process_loop, data_channel) < 0 ) {
		EXITONERROR ("");
	}

}
void process_new_shm_channel(RequestChannel* _channel) {
    nchannels ++;
    const int new_id = nchannels + 1;
	_channel->cwrite(to_string(new_id));
    RequestChannel* data_channel = new SHMRequestChannel(RequestChannel::SERVER_SIDE, new_id);
	pthread_t thread_id;
	if (pthread_create(& thread_id, NULL, handle_shm_process_loop, data_channel) < 0 ) {
		EXITONERROR ("");
	}

}

void process_fifo_request(RequestChannel* _channel, string _request) {
	//cout << "Processing Request! "<< _request <<"\n";
	if (_request.compare(0, 5, "hello") == 0) {
		_channel->cwrite("hello to you too");
	}
	else if (_request.compare(0, 4, "data") == 0) {
		usleep(1000 + (rand() % 5000));
		_channel->cwrite(to_string(rand() % 100));	
	}
	else if (_request.compare(0, 10, "newchannel") == 0) {
		 process_new_fifo_channel(_channel);
	}
	else {
		_channel->cwrite("unknown request");
	}
}
void process_shm_request(RequestChannel* _channel, string _request) {
	//cout << "Processing Request! "<< _request <<"\n";
	if (_request.compare(0, 5, "hello") == 0) {
		_channel->cwrite("hello to you too");
	}
	else if (_request.compare(0, 4, "data") == 0) {
		usleep(1000 + (rand() % 5000));
		_channel->cwrite(to_string(rand() % 100));
	}
	else if (_request.compare(0, 10, "newchannel") == 0) {
		process_new_shm_channel(_channel);
	}
	else {
		_channel->cwrite("unknown request");
	}
}
void process_mq_request(RequestChannel* _channel, string _request) {
	//cout << "Processing Request! "<< _request <<"\n";
	if (_request.compare(0, 5, "hello") == 0) {
		_channel->cwrite("hello to you too");
	}
	else if (_request.compare(0, 4, "data") == 0) {
		usleep(1000 + (rand() % 5000));
		_channel->cwrite(to_string(rand() % 100));
	}
	else if (_request.compare(0, 10, "newchannel") == 0) {
		process_new_mq_channel(_channel);
		//process_newchannel(_channel, t);
		//process_newchannel(_channel);
	}
	else {
		_channel->cwrite("unknown request");
	}
}

void* handle_fifo_process_loop (void* _channel) {
	RequestChannel* channel = (RequestChannel*)_channel;
	//cout << "Handling Request! \n";
	for(;;) {
	   // cout << "Getting request \n";
		string request = channel->cread();
		if (request.compare("quit") == 0) {
			break;                  // break out of the loop;
		}
		//process_request(channel, request);
		process_fifo_request(channel, request);

	}
}
void* handle_mq_process_loop (void* _channel) {
	RequestChannel* channel = (RequestChannel*)_channel;
	//RequestChannel* channel = (RequestChannel*)hpl;
	//cout << "Handling Request! \n";
	for(;;) {
		//cout << "Getting request \n";
		string request = channel->cread();
		if (request.compare("quit") == 0) {
			break;                  // break out of the loop;
		}
		process_mq_request(channel, request);
	}
}
void* handle_shm_process_loop (void* hpl) {
	//hpl_args* _channel = (hpl_args*)hpl;
	RequestChannel* channel = (RequestChannel*)hpl;
	//cout << "Handling Request! \n";
	for(;;) {
		//cout << "Getting request \n";
		string request = channel->cread();
		if (request.compare("quit") == 0) {
			break;                  // break out of the loop;
		}
		process_shm_request(channel, request);
	}
}


/*--------------------------------------------------------------------------*/
/* MAIN FUNCTION */
/*--------------------------------------------------------------------------*/


int main(int argc, char * argv[]) {
	RequestChannel * control_channel_instance;
	char i = 'f'; // default to fifo type
	int port = -1;
	int opt = 0;
	string open_file, close;
	open_file = close = "";
	if ( argc <=1 ) {
			printf("Running dataserver requires mandatory argument '-i', IPC type to be defined!\n");
			exit(-1);
	}
	while ((opt = getopt(argc, argv, "i:p:")) != -1) {
		switch (opt) {
			case 'i':
				i = *optarg;
				break;
			case 'p':
				port = atoi(optarg);
				break;
			default:
                const string e = "type of server " + to_string(i) + " is not known.";
                perror(e.c_str());
                exit(-2);
			}
		}
		if (i=='n' && port==-1 ) {
			printf("Running dataserver over network requires argument '-p', port number, to be defined!\n");
			exit(-2);
		}
		switch(i){
			case 'f':
				control_channel_instance = new FIFORequestChannel("control",FIFORequestChannel::SERVER_SIDE);
				newchannel_lock = PTHREAD_MUTEX_INITIALIZER;
				handle_fifo_process_loop(control_channel_instance);
				break;
			case 's':
				if( creat( SHM_PATHNAME, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH ) < 0 ) perror( "server : Error creating file" );
				control_channel_instance = new SHMRequestChannel(SHMRequestChannel::SERVER_SIDE,1);
				newchannel_lock = PTHREAD_MUTEX_INITIALIZER;
				handle_shm_process_loop(control_channel_instance);
				break;
			case 'q':
				 open_file = "touch mq";
    			 system(open_file.c_str());
				if( creat( PATHNAME, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH ) < 0 ) perror( "server : Error creating file" );
				control_channel_instance = new MQRequestChannel(MQRequestChannel::SERVER_SIDE, 1);
				newchannel_lock = PTHREAD_MUTEX_INITIALIZER;
				handle_mq_process_loop(control_channel_instance);
				break;
			case 'n':{
				control_channel_instance = new NetworkRequestChannel(NetworkRequestChannel::SERVER_SIDE, port);
				pthread_mutex_t newchannel_lock = PTHREAD_MUTEX_INITIALIZER;
				break;
			}
			default:
				const string e = "type of data server " + to_string(i) + " is not known.";
				perror(e.c_str());
				exit(-1);
		}
		//cout << "data server arguments accepted : " << *argv[1] << endl;
		// keep track of the type
		//handle_process_loop(control_channel_instance);
		//delete from the heap
		delete control_channel_instance;
		if(i=='s'){
			if( remove( SHM_PATHNAME ) != 0 ) perror( "server : Error deleting file" );
		}
		else{
			if( remove( PATHNAME ) != 0 ) perror( "server : Error deleting file" );
		}
	}

