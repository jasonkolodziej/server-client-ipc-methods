/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-12-01 14:34:21 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-12-01 14:41:37
 */
#ifndef _reqchannel_H_                   
#define _reqchannel_H_


#include <cassert>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>

#include<sys/socket.h>
#include<netinet/in.h>
#include <arpa/inet.h>


#include <iostream>
#include <fstream>
#include <exception>
#include <string>

#define MSG_EXCEPT 020000 /* recv any msg except of specified type */
#define SHM_SIZE 50
#define MSGBUFF_SIZE 256
#define SHMBUFF_SIZE MSGBUFF_SIZE
#define PATHNAME "mq"
#define QUEUE_PERMISSIONS 0660
#define SERVER_MESSAGE 1L
#define CLIENT_MESSAGE 2L
#define SHM_PATHNAME "shm"

using namespace std;

void EXITONERROR (string msg);

class RequestChannel { 
public:
	typedef enum {SERVER_SIDE, CLIENT_SIDE} Side ;
	typedef enum {READ_MODE, WRITE_MODE} Mode ;
	Side     my_side;
	/* CONSTRUCTOR/DESTRUCTOR */
RequestChannel()= default;
RequestChannel(const Side side):my_side(side){ };
//RequestChannel (const string name , const Side side );
virtual
~RequestChannel ()= default;
    
virtual string cread ( )=0;
/* Blocking read of data from the channel. Returns a string of characters read from the channel. Returns NULL if read failed. */
virtual void cwrite (string msg)=0; // *** was return int ****
/* Write the data to the channel. The function returns
the number of characters written to the channel. */

protected:
	int seed = rand() + 1;
};

#endif


