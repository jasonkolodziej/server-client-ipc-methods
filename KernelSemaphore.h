/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-11-14 09:54:00 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-11-14 13:44:42
 */
#ifndef KERNELSEMAPHORE_H
#define KERNELSEMAPHORE_H

#include "reqchannel.h"
#include <sys/sem.h>
#include <pthread.h>
/**
 * This class is a wrapper class for semaphores ( signalling mechanism )
 * at the kernel level for sychronization
 * **/
class KernelSemaphore{
    private:
    /* INTERNAL DATA STRUCTURES */
    int semid = -2;
    RequestChannel::Side caller;
    short  sem_array[4]; // 4 semaphores
    //int seed = rand() + 1;

    /*                                                               */
    /* The first semaphore in the sem set means:                     */
    /*        '1' --  The shared memory segment is being used.       */
    /*        '0' --  The shared memory segment is freed.            */
    /* The second semaphore in the sem set means:                    */
    /*        '1' --  The shared memory segment has been changed by  */
    /*                the client.                                    */
    /*        '0' --  The shared memory segment has not been         */
    /*                changed by the client.                         */
    public:
    typedef enum {FREE_C=0, EMPTY_C, FREE_S, EMPTY_S ,FREE_WAIT_C, FREE_WAIT_S} Semaphore;
    \
    /* CONSTRUCTOR/DESTRUCTOR */
    KernelSemaphore()= default;
    explicit KernelSemaphore(const int sid,const RequestChannel::Side c):semid(sid),caller(c){};
    ~KernelSemaphore(){}; // make sure to remove all allocated resources /// Might Cause issues, move to RQ
        void initialize();
        void denitialize();
        void P(const Semaphore,const RequestChannel::Mode); /* Acquire Lock*/
        void V(const Semaphore,const RequestChannel::Mode); /* Release Lock */
 };

#endif //KERNELSEMAPHORE_H