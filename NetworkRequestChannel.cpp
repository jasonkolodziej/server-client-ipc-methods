/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-12-01 14:34:26 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-12-01 14:42:19
 */
#include"NetworkRequestChannel.h"
// char _message[2000];
// char buffer[1024];

void * NetworkRequestChannel::socket_threader(void *arg){
    cout << "inside socket_threader()\n";
    NetworkRequestChannel * IN  = (NetworkRequestChannel*) arg;
    pthread_detach(pthread_self());
     while(1){
        string r = IN->cread();
        string w = "";
        if (r.compare(0, 5, "hello") == 0) {
            w = "hello to you too";
        }
        else if (r.compare(0, 4, "data") == 0) {
            usleep(1000 + (rand() % 5000));
            w = to_string(rand() % 100);
        }
        else if (r.compare("quit")){
            perror("quit was received...");
            //close(new_socket);
            break;
        }
        else {
            w = "unknown request";
        }
        // Send message to the client socket
        IN->cwrite(w);
        //sleep(1);
    }
    printf("Exiting socket_threader, with closure of socket %d \n",IN->get_soc());
    delete IN;
}






void NetworkRequestChannel::cwrite(string send_back){
        pthread_mutex_lock(&lock); ///
        //cout << "cw : " << my_side << endl;
    if(my_side==SERVER_SIDE){
        //pthread_mutex_lock(&lock);
        strcpy(buffer,send_back.c_str());
        //cout << "writing : " << string(buffer) << endl;
        //pthread_mutex_unlock(&w_lock);
        send(_soc,buffer,13,0);
        pthread_mutex_unlock(&lock);
    }
    else{
        // pthread_mutex_lock(&lock); ///
    //     while (_used)
	   //     pthread_cond_wait(&in_use, &lock);
	   // _used = (char)1;
        strcpy(_message,send_back.c_str());
        if( send(_soc , _message , strlen(_message) , 0) < 0){
            perror("Send failed");
        }
        pthread_mutex_unlock(&lock); ////
    }
}

    string NetworkRequestChannel::cread(){
       pthread_mutex_lock(&lock); ///
       // cout << "cw : " << my_side << endl;
    if(my_side==SERVER_SIDE){
        //pthread_mutex_lock(&lock); ///
        recv(_soc , _message , 2048 , 0);
        string rs(_message);
        pthread_mutex_unlock(&lock); ///
       // cout << "read : " << rs << endl;
        return rs;
    }
    else{
        //Read the message from the server into the buffer
       //pthread_mutex_lock(&lock); ///
        if(recv(_soc, buffer, 1024, 0) < 0){
            perror("Receive failed");
        }
        string rs(buffer);
        // _used = (char)0;
        // pthread_cond_broadcast(&in_use);
        pthread_mutex_unlock(&lock); ///
        return rs;
    }

}
NetworkRequestChannel::NetworkRequestChannel(const NetworkRequestChannel* obj){
    _soc = obj->_new_soc;
    _s = obj->_s;
}
NetworkRequestChannel::NetworkRequestChannel(const Side s, const int port, const string ip_addr) : RequestChannel(s){
    //Create the socket.
    _soc = socket(PF_INET, SOCK_STREAM, 0);
    //Set all bits of the padding field to 0
    memset(&server_addr, '\0', sizeof(server_addr));
    // Configure settings of the server address struct
    // Address family = Internet
    server_addr.sin_family = AF_INET;
    //Set port number, using htons function to use proper byte order
    server_addr.sin_port = htons(port);
    //Set IP address to localhost
    cout << "making connection to : " << ip_addr << " port : " << port << endl;
    server_addr.sin_addr.s_addr = inet_addr(ip_addr.c_str());

    if (my_side==SERVER_SIDE){
        //Bind the address struct to the socket
        bind(_soc, (struct sockaddr *) &server_addr, sizeof(server_addr));
        //Listen on the socket, with 40 max connection requests queued
        if(listen(_soc,5)==0){
            printf("Listening\n");
        }
        else{
            perror("Server failed to listen.");
        }
        while(1) {
            //Accept call creates a new socket for the incoming connection
            _addr_size = sizeof client_addr;
            //socket_threader_args s;
            _new_soc = accept(_soc, (struct sockaddr *) &client_addr, &_addr_size);
            cout << "new accepting connection " << _new_soc << " @" << inet_ntoa(client_addr.sin_addr) << endl;
            //for each client request creates a thread and assign the client request to it to process
            //so the main thread can entertain next request
            NetworkRequestChannel * nc = new NetworkRequestChannel(this);
            if( pthread_create(&server_thread, NULL, socket_threader, nc) != 0 )
                perror("Failed to create socket thread");
        }
    }
    else {
        //Connect the socket to the server using the address
        _addr_size = sizeof server_addr;
        if(connect(_soc, (struct sockaddr *) &server_addr, _addr_size)<0){
            perror("Failed to connect to server!");
        }
    }
}