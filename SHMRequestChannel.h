/*
 * @Author: Jason A. Kolodziej
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved.
 * @Date: 2018-11-14 09:54:00
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-11-14 13:44:42
 */

#ifndef SHMREQUESTCHANNEL_H
#define SHMREQUESTCHANNEL_H


#include "reqchannel.h"
#include "KernelSemaphore.h"


class SHMBoundedBuffer{
public:
    void * data_1;
    void * data_2;
    int shmid; // shared memory segment ID
    KernelSemaphore sem;
    SHMBoundedBuffer()= default;
    explicit SHMBoundedBuffer(const int semid, const RequestChannel::Side c){
        sem = KernelSemaphore(semid, c);
    };
};

class SHMRequestChannel : public RequestChannel {

private:
    /*  The current implementation uses shared memory. */
    key_t 	 key;
    int semid;
    const int id;
    SHMBoundedBuffer shm_buf;
public:

    /* -- CONSTRUCTOR/DESTRUCTOR */

    SHMRequestChannel(const Side, const int);
    /* Creates a "local copy" of the channel specified by the given name.
     If the channel does not exist, the associated IPC mechanisms are
     created. If the channel exists already, this object is associated with the channel.
     The channel has two ends, which are conveniently called "SERVER_SIDE" and "CLIENT_SIDE".
     If two processes connect through a channel, one has to connect on the server side
     and the other on the client side. Otherwise the results are unpredictable.

     NOTE: If the creation of the request channel fails (typically happens when too many
     request channels are being created) and error message is displayed, and the program
     unceremoniously exits.

     NOTE: It is easy to open too many request channels in parallel. In most systems,
     limits on the number of open files per process limit the number of established
     request channels to 125.
    */

    ~SHMRequestChannel( ) override;
    /* Destructor of the local copy of the bus. By default, the Server Side deletes any IPC
     mechanisms associated with the channel. */

    string cread( );
    /* Blocking read of data from the channel. Returns a string of characters
     read from the channel. Returns NULL if read failed. */

    void cwrite(string);
    /* Write the data to the channel. The function returns NULL */
};

#endif //SHMREQUESTCHANNEL_H
