/*
 * @Author: Jason A. Kolodziej
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved.
 * @Date: 2018-11-14 09:54:00
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-11-14 13:44:42
 */
#include "SHMRequestChannel.h"
#include "KernelSemaphore.h"

/*--------------------------------------------------------------------------*/
/* DEFINITIONS FOR FUNCTIONS OF   SHMRequestChannel				     	    */
/*--------------------------------------------------------------------------*/


SHMRequestChannel::SHMRequestChannel(const Side _side, const int id):RequestChannel(_side),id(id){
    // ftok to generate unique key
    if ( (key = ftok(SHM_PATHNAME, id)) == (key_t)-1 )
    {
        const string side = ((RequestChannel::my_side == SERVER_SIDE) ? "server" : "client");
        const string e = "failed to create key =>" + side +
                         " key value :" + to_string(key) + ", errno " + to_string(errno);
        perror(e.c_str());
    }
    //Server side
    if(RequestChannel::my_side==SERVER_SIDE){
        semid = semget(key, 4, 0666 | IPC_CREAT);
        if (semid == -1){
            const string side = ((RequestChannel::my_side == SERVER_SIDE) ? "server" : "client");
            const string e = "failed to create semid =>" + side
                             + " : with key = " + to_string(key) + ", errno " + to_string(errno);
            perror(e.c_str());
        }
        shm_buf = SHMBoundedBuffer(semid,RequestChannel::my_side);
        shm_buf.sem.initialize();
        shm_buf.shmid = shmget(key, SHM_SIZE, 0666 | IPC_CREAT);
        if (shm_buf.shmid == -1){
            const string side = ((RequestChannel::my_side == SERVER_SIDE) ? "server" : "client");
            const string e = "failed to create shmid =>" + side
                             + " : with key = " + to_string(key) + ", errno " + to_string(errno);
            perror(e.c_str());
        }
    }
    else{
        semid = semget(key, 4, 0666);
        if (semid  == -1){
            const string side = ((RequestChannel::my_side == SERVER_SIDE) ? "server" : "client");
            const string e = "failed to create semid =>" + side
                             + " : with key = " + to_string(key) + ", errno " + to_string(errno);
            perror(e.c_str());
        }
        shm_buf = SHMBoundedBuffer(semid, RequestChannel::my_side);
        shm_buf.shmid = shmget(key, SHM_SIZE, 0666);
        if (shm_buf.shmid == -1){
            const string side = ((RequestChannel::my_side == SERVER_SIDE) ? "server" : "client");
            const string e = "failed to create shmid =>" + side
                             + " : with key = " + to_string(key) + ", errno " + to_string(errno);
            perror(e.c_str());
        }
    }
    /* attach to the segment to get a pointer to it: */
    shm_buf.data_1 = shmat(shm_buf.shmid,(void *)0, 0);
    shm_buf.data_2 = shmat(shm_buf.shmid,(void *)0, 0);
    if ( shm_buf.data_1 == NULL || shm_buf.data_2 == NULL )
    {
        const string side = ((RequestChannel::my_side == SERVER_SIDE) ? "server" : "client");
        const string e = "failed to attach to shared memory =>" + side
                         + " : with key = " + to_string(key) + ", errno " + to_string(errno);
        perror(e.c_str());
    }
}

SHMRequestChannel::~SHMRequestChannel(){
    // destroy the shared memory from the server side
    int r = shmdt(shm_buf.data_1);
    int rr = shmdt(shm_buf.data_2);
    if (r==-1 || rr==-1)
    {
        const string side = ((RequestChannel::my_side == SERVER_SIDE) ? "server" : "client");
        const string e = "failed to detach from shared memory =>" + side
                         + " : with key = " + to_string(key) + ", errno " + to_string(errno);
        perror(e.c_str());
    }
   if (RequestChannel::my_side==SERVER_SIDE){
       shm_buf.sem.denitialize();
       shmctl(shm_buf.shmid,IPC_RMID,NULL);
   }
}

string SHMRequestChannel::cread(void){
    if(RequestChannel::my_side==SERVER_SIDE){
        shm_buf.sem.P(KernelSemaphore::EMPTY_S,RequestChannel::READ_MODE);
        shm_buf.sem.V(KernelSemaphore::FREE_S,RequestChannel::READ_MODE);
        const string rt = string((char*)shm_buf.data_1);
        shm_buf.sem.P(KernelSemaphore::FREE_S, RequestChannel::READ_MODE);
        return rt;
    }
    else{
        shm_buf.sem.P(KernelSemaphore::EMPTY_C,RequestChannel::READ_MODE);
        shm_buf.sem.V(KernelSemaphore::FREE_C,RequestChannel::READ_MODE);
        const string rt = string((char*)shm_buf.data_2);
        shm_buf.sem.P(KernelSemaphore::FREE_C, RequestChannel::READ_MODE);
        return rt;
    }
}

void SHMRequestChannel::cwrite(string msg){
    if(RequestChannel::my_side==CLIENT_SIDE){
        shm_buf.sem.P(KernelSemaphore::FREE_WAIT_S, RequestChannel::WRITE_MODE); //wait for initial set value on FREE
        shm_buf.sem.V(KernelSemaphore::FREE_S, RequestChannel::WRITE_MODE);
        /* modify segment by copying a string msg into it */
        strncpy((char*)shm_buf.data_1, msg.c_str(), SHM_SIZE);
        shm_buf.sem.P(KernelSemaphore::FREE_S, RequestChannel::WRITE_MODE);
        shm_buf.sem.V(KernelSemaphore::EMPTY_S, RequestChannel::WRITE_MODE);
    }
    else{
        shm_buf.sem.P(KernelSemaphore::FREE_WAIT_C, RequestChannel::WRITE_MODE); //wait for initial set value on FREE
        shm_buf.sem.V(KernelSemaphore::FREE_C, RequestChannel::WRITE_MODE);
        /* modify segment by copying a string msg into it */
        strncpy((char*)shm_buf.data_2, msg.c_str(), SHM_SIZE);
        shm_buf.sem.P(KernelSemaphore::FREE_C, RequestChannel::WRITE_MODE);
        shm_buf.sem.V(KernelSemaphore::EMPTY_C, RequestChannel::WRITE_MODE);
    }
}

/*--------------------------------------------------------------------------*/
/* END OF THE   SHMRequestChannel  CLASS								    */
/*--------------------------------------------------------------------------*/