/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-12-01 14:34:21 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-12-01 14:41:37
 */

#include "reqchannel.h"
#include <pthread.h>
#ifndef NETWORKREQUEST_H
#define NETWORKREQUEST_H


class NetworkRequestChannel : public RequestChannel {
private:
    static void * socket_threader(void *arg);
    // struct socket_threader_args{
    //     char _message[MESSAGE_LENGTH];
    //     char buffer[MESSAGE_LENGTH];
    //     int _new_soc;
    // };
    Side _s;
        char _message[2048];
        char buffer[1024];
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    int _soc, _new_soc;
    socklen_t _addr_size;
    pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
// 	pthread_cond_t in_use = PTHREAD_COND_INITIALIZER;
// 	char _used = (char)0;
    pthread_t server_thread;
public:
    explicit NetworkRequestChannel(const Side, const int, const string = "127.0.0.1");
    NetworkRequestChannel(const NetworkRequestChannel*);
    ~NetworkRequestChannel()override {
        close(_soc);
    };
    int get_new_socket(void);
    void cwrite(string);
    const int get_soc()const {return this->_soc;}
    string cread();
};

#endif