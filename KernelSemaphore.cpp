/*
 * @Author: Jason A. Kolodziej
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved.
 * @Date: 2018-11-14 09:54:00
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-11-14 13:44:42
 */

#include "KernelSemaphore.h"


void KernelSemaphore::initialize() {
        sem_array[0] = 0;
        sem_array[1] = 0;
        sem_array[2] = 0;
        sem_array[3] = 0;
        if(semctl( semid, 1, SETALL, sem_array) == -1)
        {
            perror("KernelSemaphore(): semctl() initialize() failed");
        }
    }

void KernelSemaphore::denitialize() {
    if(caller==RequestChannel::SERVER_SIDE && semid != -2){
        if (semctl( semid, 1, IPC_RMID )==-1) {
            perror("KernelSemaphore(): semctl() denitialize() remove id failed");
        }
    }
}

void KernelSemaphore::P(const Semaphore s, const RequestChannel::Mode m) { //decrease val or LOCK
    int flag = (m==RequestChannel::Mode::READ_MODE && (s==Semaphore::FREE_C || s==Semaphore::FREE_S)) ? IPC_NOWAIT : 0;
    struct sembuf sb;
    if(m==RequestChannel::WRITE_MODE && (s==Semaphore::FREE_WAIT_C || s==Semaphore::FREE_WAIT_S)){
        sb = {0,0,0};
    }
    else{
        sb = {(unsigned short)s, -1, (short)flag };
    }
    if (semop(semid, &sb, 1) == -1){
        const string sem = (s==Semaphore::EMPTY_S || s==Semaphore::EMPTY_C) ? "EMPTY" : "FREE";
        const string side = (caller==RequestChannel::Side::SERVER_SIDE) ? "SERVER" : "CLIENT";
        const string e = "KernelSemaphore(): semop() failed at P() => Semaphore: " + sem + ", Caller: " + side;
        perror(e.c_str());

    }
}

void KernelSemaphore::V(const Semaphore s, const RequestChannel::Mode m) { //increase val or UNLOCK
    int flag = (m==RequestChannel::Mode::READ_MODE && (s==Semaphore::FREE_C || s==Semaphore::FREE_S)) ? IPC_NOWAIT : 0;
    struct sembuf sb;
    if(m==RequestChannel::WRITE_MODE && (s==Semaphore::FREE_WAIT_C || s==Semaphore::FREE_WAIT_S)){
        sb = {0,0,0};
    }
    else{
        sb = {(unsigned short)s, 1, (short)flag };
    }
    if (semop(semid, &sb, 1) == -1){
        const string sem = (s==Semaphore::EMPTY_S || s==Semaphore::EMPTY_C) ? "EMPTY" : "FREE";
        const string side = (caller==RequestChannel::Side::SERVER_SIDE) ? "SERVER" : "CLIENT";
        const string e = "KernelSemaphore(): semop() failed at V() => Semaphore: " + sem + ", Caller: " + side;
        perror(e.c_str());

    }
}
