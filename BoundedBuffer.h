/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-12-01 14:34:21 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-12-01 14:41:37
 */
#ifndef BoundedBuffer_h
#define BoundedBuffer_h

#include <stdio.h>
#include <queue>
#include <thread>
#include <string>
using namespace std;

class BoundedBuffer {
private:
    int buff_size =0;
	pthread_mutex_t  mutx = PTHREAD_MUTEX_INITIALIZER;
	pthread_cond_t consumer_var = PTHREAD_COND_INITIALIZER;
    pthread_cond_t producer_var = PTHREAD_COND_INITIALIZER;

    int ret;
	queue<string> q;	
public:
    BoundedBuffer(int);
	~BoundedBuffer();
	int size();
    void push (string);
    string pop();
};

#endif /* BoundedBuffer_ */
