# makefile
# makefile

all: dataserver client

FIFORequestChannel.o: reqchannel.h FIFORequestChannel.h FIFORequestChannel.cpp
	g++ -g -w -Wall -O1 -std=c++11 -c FIFORequestChannel.cpp

KernelSemaphore.o: reqchannel.h KernelSemaphore.h KernelSemaphore.cpp
	g++ -g -w -Wall -O1 -std=c++11 -c KernelSemaphore.cpp

SHMRequestChannel.o: reqchannel.h SHMRequestChannel.h SHMRequestChannel.cpp
	g++ -g -w -Wall -O1 -std=c++11 -c SHMRequestChannel.cpp

MQRequestChannel.o: reqchannel.h MQRequestChannel.h MQRequestChannel.cpp
	g++ -g -w -Wall -O1 -std=c++11 -c MQRequestChannel.cpp

NetworkRequestChannel.o: reqchannel.h NetworkRequestChannel.h NetworkRequestChannel.cpp
	g++ -g -w -Wall -O1 -std=c++11 -c NetworkRequestChannel.cpp

BoundedBuffer.o: BoundedBuffer.h BoundedBuffer.cpp
	g++ -g -w -Wall -O1 -std=c++11 -c BoundedBuffer.cpp

Histogram.o: Histogram.h Histogram.cpp
	g++ -g -w -Wall -O1 -std=c++11 -c Histogram.cpp


dataserver: dataserver.cpp SHMRequestChannel.o MQRequestChannel.o FIFORequestChannel.o NetworkRequestChannel.o KernelSemaphore.o
	g++ -g -w -Wall -O1 -std=c++11 -o dataserver dataserver.cpp FIFORequestChannel.o NetworkRequestChannel.o SHMRequestChannel.o KernelSemaphore.o MQRequestChannel.o -lpthread

client: client.cpp SHMRequestChannel.o MQRequestChannel.o FIFORequestChannel.o NetworkRequestChannel.o BoundedBuffer.o Histogram.o KernelSemaphore.o
	g++ -g -w -Wall -O1 -std=c++11 -o client client.cpp FIFORequestChannel.o NetworkRequestChannel.o SHMRequestChannel.o MQRequestChannel.o KernelSemaphore.o BoundedBuffer.o Histogram.o -lpthread

clean:
	rm -rf *.o fifo* dataserver client
